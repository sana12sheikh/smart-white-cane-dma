import cv2
import numpy as np
import gtts
from playsound import playsound


cap = cv2.VideoCapture(-1)
whT = 320
confThreshold = 0.5
nmsThreshold = 0.3


class my_dictionary(dict):

    # __init__ function
    def __init__(self):
        self = dict()

        # Function to add key:value

    def add(self, key, value):
        self[key] = value

graph_dict = my_dictionary()
    # classes = ['', '']
classesFile = 'coco.names'
classNames = []
with open(classesFile, 'rt') as f:
    classNames = f.read().rstrip('\n').split('\n')
# print(classNames)
# print(len(classNames))
modelConfiguration = 'yolov3-tiny.cfg'
modelWeights = 'yolov3-tiny.weights'
net = cv2.dnn.readNetFromDarknet(modelConfiguration, modelWeights)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

def findObjects(outputs, img):
    hT, wT, cT = img.shape
    bbox = []
    classIds = [] #bounding boxes
    confs = [] # confidence values, whenever we find a good object detection we will put the value of box in these lists
    for output in outputs:
        for det in output:
            scores = det[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            #filtering objects
            if confidence > confThreshold:
                # we know width is 3rd element and heigth is 4th element
                w,h = int(det[2]*wT), int(det[3]*hT) # these are in decimal as its percentsge so multiplication is required
                x,y = int(det[0]*wT - w/2), int(det[1]*hT - h/2) #this is not x,y corner point. these are origin points. We will divide width by 2 and subrat and similar for height
                bbox.append([x,y,w,h])
                classIds.append(classId)
                confs.append(float(confidence))
    print(len(bbox)) #with my mouse its detecting 2 bounding boxes
    indices = cv2.dnn.NMSBoxes(bbox,confs,confThreshold,nmsThreshold)
    for i in indices:
        box = bbox[i]
        x,y,w,h = box[0], box[1], box[2], box[3]
        cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 255), 2)
        cv2.putText(img,f'{classNames[classIds[i]].upper()} {int(confs[i]*100)}%', (x,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 255), 2)
        # temp1 = classNames[classIds[i]].upper()
        # if classNames[classIds[i]].upper() == classNames[classIds[i-1]].upper():
        #     pass
        # else:
        #     print('not equal')
        tts = gtts.gTTS(classNames[classIds[i]])
        tts.save("test.mp3")
        playsound("test.mp3")
        if classNames[classIds[i]].upper() in graph_dict:
            graph_dict[classNames[classIds[i]].upper()] += 1
        else:
            graph_dict.add(classNames[classIds[i]].upper(), 1)


while True:
    success, img = cap.read()
    blob = cv2.dnn.blobFromImage(img,1/255,(whT,whT),[0,0,0],1,crop=False)
    net.setInput(blob)
    layerNames = net.getLayerNames()
    # print(layerNames)
    # print(net.getUnconnectedOutLayers())
    outputNames = [layerNames[i-1] for i in net.getUnconnectedOutLayers()]
    # print(outputNames)
    outputs = net.forward(outputNames)
    # print(type(outputs[0]))
    # print(outputs[0].shape) #first layer of output produces this num of bounding boxes
    # print(outputs[0].shape)
    # print(outputs[0].shape)
    # print(outputs[0][0])
    findObjects(outputs,img)


    cv2.imshow('Image', img)
    cv2.waitKey(1)
